import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Transactionscreen from './screen/Transactionscreen';
import DetailTransactionscreen from './screen/Detailtransaction';
import Checkoutscreen from './screen/Checkoutscreen';

const Stack = createStackNavigator();

function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Transaction"
        component={Transactionscreen}
        options={{headerTitle: 'Transaksi', headerShown: true}}
      />
      <Stack.Screen
        name="Detail"
        component={DetailTransactionscreen}
        options={{headerTitle: '', headerShown: true}}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkoutscreen}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

export default TransactionNavigation;
