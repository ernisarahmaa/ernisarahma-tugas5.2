import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Homescreen from './screen/Homescreen';
import Detailscreen from './screen/Detailscreen';
import Orderscreen from './screen/Orderscreen';
import Cartscreen from './screen/Cartscreen';
import DetailCartscreen from './screen/Detailcart';
import Reservasiscreen from './screen/Reservasi';

const Stack = createStackNavigator();

function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Homescreen} />
      <Stack.Screen name="Detail" component={Detailscreen} />
      <Stack.Screen
        name="Order Form"
        component={Orderscreen}
        options={{headerTitle: 'Formulir Pemesanan', headerShown: true}}
      />
      <Stack.Screen
        name="Cart"
        component={Cartscreen}
        options={{headerTitle: 'Keranjang', headerShown: true}}
      />
      <Stack.Screen
        name="Detail Cart"
        component={DetailCartscreen}
        options={{headerTitle: 'Summary', headerShown: true}}
      />
      <Stack.Screen name="Reservasi" component={Reservasiscreen} />
    </Stack.Navigator>
  );
}

export default HomeNavigation;
