import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';

const Profilescreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: '#FFF',
          paddingHorizontal: 25,
          paddingVertical: 50,
          justifyContent: 'space-between',
          height: 295,
          alignItems: 'center',
        }}>
        <Image
          style={{width: 95, height: 95}}
          source={require('../assets/icons/profileuser.png')}
        />
        <View style={{alignItems: 'center', marginVertical: 22}}>
          <Text style={{color: '#050152', fontWeight: '700', fontSize: 20}}>
            Agil Bani
          </Text>
          <Text style={{color: '#A8A8A8', fontSize: 10}}>
            gilagil@gmail.com
          </Text>
        </View>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('ProfileNavigation', {screen: 'Edit Profile'})
          }
          style={{
            backgroundColor: '#F6F8FF',
            width: 65,
            height: 28,
            borderRadius: 18,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              textAlign: 'center',
            }}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{marginHorizontal: 18}}>
        <View
          style={{
            backgroundColor: '#FFF',
            marginVertical: 18,
            paddingHorizontal: 80,
          }}>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginVertical: 18,
              }}>
              About
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginVertical: 18,
              }}>
              Terms & Condition
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'FAQ'})
            }>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginVertical: 18,
              }}>
              FAQ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginVertical: 18,
              }}>
              History
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginVertical: 18,
              }}>
              Setting
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={{
            flexDirection: 'row',
            backgroundColor: '#FFF',
            width: '100%',
            justifyContent: 'center',
            height: 50,
            alignItems: 'center',
          }}>
          <Image
            style={{width: 24, height: 24, marginRight: 5}}
            source={require('../assets/icons/logout.png')}
          />
          <Text
            style={{
              color: '#EA3D3D',
              fontSize: 16,
              fontWeight: '500',
              textAlign: 'center',
            }}>
            Log out
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Profilescreen;
