import {combineReducers} from 'redux';
import reservationReducer from './reservationReducer';

const rootReducer = combineReducers({
  reservations: reservationReducer,
  // other reducers here
});

export default rootReducer;
