export const ADD_RESERVATION = 'ADD_RESERVATION';

export const addReservation = reservationData => {
  return {
    type: ADD_RESERVATION,
    payload: reservationData,
  };
};
