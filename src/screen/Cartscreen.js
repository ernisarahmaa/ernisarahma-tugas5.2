import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';

const Cartscreen = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            marginTop: 9,
            backgroundColor: '#fff',
            flexDirection: 'row',
            paddingVertical: 25,
            marginHorizontal: 11,
            borderRadius: 8,
          }}>
          <Image
            style={{width: 85, height: 85, marginHorizontal: 11}}
            source={require('../assets/images/shoes1.png')}
          />
          <View style={{justifyContent: 'space-evenly'}}>
            <Text style={{fontWeight: '500', color: '#000'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text>Cuci Sepatu</Text>
            <Text>Note : -</Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            marginVertical: 45,
          }}>
          <Image
            style={{width: 20, height: 20}}
            source={require('../assets/icons/plus.png')}
          />
          <Text style={{marginLeft: 8, color: '#BB2427', fontWeight: '700'}}>
            Tambah Barang
          </Text>
        </TouchableOpacity>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Detail Cart')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Selanjutnya
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Cartscreen;
