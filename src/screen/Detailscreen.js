import React from 'react';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const Detailscreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <ImageBackground
          source={require('../assets/images/detailava2.png')}
          resizeMode="cover"
          style={{height: 317}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 25,
              marginHorizontal: 20,
            }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('BottomNavigation', {screen: 'Home'})
              }>
              <Image
                style={{width: 35, height: 35, tintColor: 'white'}}
                source={require('../assets/icons/back.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('../assets/icons/cart.png')}
                style={{
                  width: 25,
                  height: 30,
                  tintColor: 'white',
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>

        <View
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingTop: 38,
            marginTop: -20,
          }}>
          <View
            style={{
              paddingHorizontal: 20,
            }}>
            <Text
              style={{
                color: '#0A0827',
                fontWeight: '700',
                fontSize: 24,
                marginBottom: 5,
              }}>
              Jack Repair Seturan
            </Text>
            <Image
              style={{width: '19%', height: 15, resizeMode: 'contain'}}
              source={require('../assets/icons/stars.png')}
            />
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
              }}>
              <View>
                <Image
                  style={{width: 20, height: 20}}
                  source={require('../assets/icons/location.png')}
                />
              </View>
              <View style={{width: '60%', marginLeft: 10}}>
                <Text>
                  Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384
                </Text>
              </View>
              <TouchableOpacity
                style={{justifyContent: 'center', marginLeft: 'auto'}}>
                <Text
                  style={{color: '#3471CD', fontWeight: '700', fontSize: 15}}>
                  Lihat Maps
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                alignItems: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#DFEEE5',
                  width: 58,
                  height: 25,
                  borderRadius: 15,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: '#2CAD60',
                    textAlign: 'center',
                    fontSize: 15,
                    fontWeight: '700',
                  }}>
                  BUKA
                </Text>
              </View>
              <Text
                style={{
                  color: '#343434',
                  fontSize: 15,
                  fontWeight: '700',
                  marginLeft: 25,
                }}>
                09:00 - 21:00
              </Text>
            </View>
          </View>

          <View
            style={{
              borderTopWidth: 1,
              borderColor: '#EEEEEE',
              marginTop: 15,
            }}>
            <View
              style={{
                paddingHorizontal: 20,
              }}>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 25,
                }}>
                Deskripsi
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  marginTop: 13,
                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
                gravida mattis arcu interdum lectus egestas scelerisque. Blandit
                porttitor diam viverra amet nulla sodales aliquet est. Donec
                enim turpis rhoncus quis integer. Ullamcorper morbi donec
                tristique condimentum ornare imperdiet facilisi pretium
                molestie.
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 25,
                }}>
                Range Biaya
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 5,
                }}>
                Rp 20.000 - 80.000
              </Text>
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: 20,
            }}>
            <TouchableOpacity
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 77,
              }}
              onPress={() => navigation.navigate('Order Form')}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Repair Disini
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default Detailscreen;
