import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStore, applyMiddleware} from 'redux';

import {persistStore, persistReducer} from 'redux-persist';
import thunk from 'redux-thunk';
// Imports: Redux
import rootReducer from './screen/combineReducers';
const persistConfig = {
  // Root
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  timeout: null,
};

// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);
// Redux: Store
const store = createStore(
  persistedReducer,
  applyMiddleware(
    thunk,
    // createLogger(),
  ),
);
// Middleware: Redux Persist Persister
const persistor = persistStore(store);

export {store, persistor};
